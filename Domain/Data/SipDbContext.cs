﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Data
{
    public class SipDbContext : DbContext
    {
        public SipDbContext(DbContextOptions<SipDbContext> options) : base(options)
        {

        }
        
        public DbSet<DbStop> Stops { get; set; }
    }
}
