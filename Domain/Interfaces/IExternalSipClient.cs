﻿using MarcinWork.Sip.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Interfaces
{
    public interface IExternalSipClient
    {
        List<Line> GetLines();

        Line GetLine(int lineId, bool withRoutes);

        List<Stop> GetStops();

        Stop GetStop(string stopCode, bool withLiveSchedule);

        List<LiveScheduleItem> GetLiveSchedule(string stopCode);
    }
}
