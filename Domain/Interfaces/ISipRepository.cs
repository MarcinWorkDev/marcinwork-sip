﻿using MarcinWork.Sip.Domain.Data;
using MarcinWork.Sip.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Interfaces
{
    public interface ISipRepository
    {
        DbStop GetDbStop(string userIdentifier, Guid id);

        List<DbStop> GetDbStops(string userIdentifier);

        DbStop AddDbStop(string userIdentifier, string stopCode, string name);

        DbStop ChangeNameDbStop(string userIdentifier, Guid id, string name);

        void DeleteStop(string userIdentifier, Guid id);
    }
}
