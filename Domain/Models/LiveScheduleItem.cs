﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Models
{
    public class LiveScheduleItem
    {
        public string Line { get; set; }
        public string Destination { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public short Minutes { get; set; }
        public string Properties { get; set; }
    }
}
