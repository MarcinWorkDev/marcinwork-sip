﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Models
{
    public class Route
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Stop> Stops { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }
}
