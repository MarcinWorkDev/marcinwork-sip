﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Models
{
    public class Line
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public string Route { get; set; }

        public List<Route> Routes { get; set; }
    }
}
