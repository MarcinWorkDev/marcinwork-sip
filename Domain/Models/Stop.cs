﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Models
{
    public class Stop
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string NameShort { get; set; }
        public List<LiveScheduleItem> LiveSchedule { get; set; }
    }
}
