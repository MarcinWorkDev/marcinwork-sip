﻿using MarcinWork.Sip.Domain.Data;
using MarcinWork.Sip.Domain.Interfaces;
using MarcinWork.Sip.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace MarcinWork.Sip.Domain.Services
{
    public class SipRepository : ISipRepository
    {
        private readonly SipDbContext _dbContext;
        
        public SipRepository(SipDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public DbStop AddDbStop(string userIdentifier, string stopCode, string name)
        {
            var dbStop = new DbStop
            {
                Id = Guid.NewGuid(),
                UserId = userIdentifier,
                CreatedAt = DateTime.Now,
                StopCode = stopCode,
                Name = name
            };
            _dbContext.Add(dbStop);
            _dbContext.SaveChanges();
            return dbStop;
        }
        
        public DbStop ChangeNameDbStop(string userIdentifier, Guid id, string name)
        {
            var dbStop = this.GetDbStop(userIdentifier, id);
            dbStop.ModifiedAt = DateTime.Now;
            dbStop.Name = name;

            _dbContext.Update(dbStop);
            _dbContext.SaveChanges();
            return dbStop;
        }

        public void DeleteStop(string userIdentifier, Guid id)
        {
            var dbStop = this.GetDbStop(userIdentifier, id);
            dbStop.DeletedAt = DateTime.Now;

            _dbContext.Update(dbStop);
            _dbContext.SaveChanges();
        }

        public DbStop GetDbStop(string userIdentifier, Guid id)
        {
            return _dbContext.Stops.Where(x => x.UserId == userIdentifier && x.Id == id).FirstOrDefault();
        }

        public List<DbStop> GetDbStops(string userIdentifier)
        {
            return _dbContext.Stops.Where(x => x.UserId == userIdentifier && !x.DeletedAt.HasValue).ToList();
        }
        
        /*private DbUser GetCurrentUser(Guid? externalUserIdentifier)
        {
            if (!externalUserIdentifier.HasValue)
            {
                return default(DbUser);
            }

            var user = _dbContext.Users.Where(x => x.ExternalNameIdentifier == externalUserIdentifier).FirstOrDefault();

            if (user == default(DbUser))
            {
                user = new DbUser
                {
                    Id = Guid.NewGuid(),
                    ExternalNameIdentifier = externalUserIdentifier.Value,
                    CreatedAt = DateTime.Now
                };
                _dbContext.Users.Add(user);
                _dbContext.SaveChanges();
            }

            return user;
        }*/
    }
}
