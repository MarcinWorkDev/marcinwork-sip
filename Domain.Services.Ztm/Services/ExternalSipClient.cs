﻿using MarcinWork.Sip.Domain.Interfaces;
using MarcinWork.Sip.Domain.Models;
using MarcinWork.Sip.Domain.Services.Ztm.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;

namespace MarcinWork.Sip.Domain.Services.Ztm.Services
{
    public class ExternalSipClient : IExternalSipClient
    {
        private ZtmApiClient _sipClient;
        private IMemoryCache _cache;
        
        public ExternalSipClient(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _sipClient = new ZtmApiClient();
        }

        public List<Line> GetLines()
        {
            var linesCache = _cache.GetOrCreate("Lines", entry => 
            {
                var linesDto = _sipClient.GetLines();
                var lines = new List<Line>();

                foreach (var item in linesDto ?? new List<LineDto>())
                {
                    var line = new Line
                    {
                        Id = item.Id,
                        Number = item.Number,
                        Route = item.Route
                    };
                    lines.Add(line);
                }

                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(2);

                return lines;
            });
            
            return linesCache;
        }

        public Line GetLine(int lineId, bool withRoutes)
        {
            var lineDto = _sipClient.GetLines().Where(x => x.Id == lineId).FirstOrDefault();
            var line = new Line
            {
                Id = lineDto.Id,
                Number = lineDto.Number,
                Route = lineDto.Route
            };

            if (withRoutes)
            {
                var routesDto = _sipClient.GetRoutes(lineDto.Id);
                var routes = new List<Route>();
                foreach (var routeDto in routesDto ?? new List<RouteDto>())
                {
                    var route = new Route
                    {
                        Id = routeDto.Id,
                        Name = routeDto.Name,
                        Stops = new List<Stop>(),
                        Origin = routeDto.Origin,
                        Destination = routeDto.Destination
                    };
                    
                    foreach (var stopDto in routeDto.Stops ?? new List<StopDto>())
                    {
                        var stop = new Stop
                        {
                            Id = stopDto.Id,
                            Code = stopDto.Code,
                            Name = stopDto.Name,
                            Latitude = stopDto.Latitude,
                            Longitude = stopDto.Longitude,
                            NameShort = stopDto.NameShort
                        };
                        route.Stops.Add(stop);
                    }
                    routes.Add(route);
                }
                line.Routes = routes;
            }
            return line;
        }

        public List<Stop> GetStops()
        {
            var stopsCache = _cache.GetOrCreate("Stops", entry =>
            {
                var linesDto = _sipClient.GetLines();
                var stopsDto = new List<StopDto>();

                foreach (var lineDto in linesDto ?? new List<LineDto>())
                {
                    foreach (var routeDto in _sipClient.GetRoutes(lineDto.Id) ?? new List<RouteDto>())
                    {
                        foreach (var stopDto in routeDto.Stops)
                        {
                            if (stopsDto.Where(x => x.Id == stopDto.Id).Count() == 0)
                            {
                                stopsDto.Add(stopDto);
                            }
                        }
                    }
                }

                var stops = new List<Stop>();

                foreach (var stopDto in stopsDto ?? new List<StopDto>())
                {
                    var stop = new Stop
                    {
                        Id = stopDto.Id,
                        Code = stopDto.Code,
                        Name = stopDto.Name,
                        Latitude = stopDto.Latitude,
                        Longitude = stopDto.Longitude,
                        NameShort = stopDto.NameShort
                    };
                    stops.Add(stop);
                }

                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(2);
                entry.SetValue(stops);

                return stops;
            });

            if (stopsCache.Count == 0)
            {
                _cache.Remove("Stops");
                this.GetStops();
            }
            
            return stopsCache;
        }

        public List<LiveScheduleItem> GetLiveSchedule(string stopCode)
        {
            var liveSchedulesDto = _sipClient.GetLiveScheduleItems(stopCode);
            var liveSchedule = new List<LiveScheduleItem>();
            foreach (var liveScheduleItemDto in liveSchedulesDto ?? new List<LiveScheduleItemDto>())
            {
                var liveScheduleItem = new LiveScheduleItem
                {
                    Line = liveScheduleItemDto.Line,
                    Destination = liveScheduleItemDto.Destination,
                    ArrivalTime = liveScheduleItemDto.ArrivalTime,
                    Minutes = liveScheduleItemDto.Minutes,
                    Properties = liveScheduleItemDto.Properties
                };
                liveSchedule.Add(liveScheduleItem);
            }
            return liveSchedule;
        }

        public Stop GetStop(string stopCode, bool withLiveSchedule)
        {
            var stop = this.GetStops().Where(x => x.Code == stopCode).FirstOrDefault();

            if (withLiveSchedule)
            {
                stop.LiveSchedule = GetLiveSchedule(stopCode);
            }
            return stop;
        }
    }
}
