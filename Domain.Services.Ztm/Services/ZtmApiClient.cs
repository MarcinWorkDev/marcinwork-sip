﻿using MarcinWork.Sip.Domain.Interfaces;
using MarcinWork.Sip.Domain.Models;
using MarcinWork.Sip.Domain.Services.Ztm.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Services.Ztm.Services
{
    public class ZtmApiClient
    {
        public List<Models.LiveScheduleItemDto> GetLiveScheduleItems(string stopCode)
        {
            var stopTableDto = GetResponse<List<Models.LiveScheduleItemDto>>("sip_get_llegadas_parada", stopCode, null);
            return stopTableDto;
        }

        public List<LineDto> GetLines()
        {
            var linesDto = GetResponse<List<LineDto>>("sip_get_lineas", null, null);
            return linesDto;
        }

        public List<StopDto> GetStops(int lineId)
        {
            var stopsDto = GetResponse<List<StopDto>>("sip_get_paradas", null, lineId);
            return stopsDto;
        }

        public List<RouteDto> GetRoutes(int lineId)
        {
            var routesDto = GetResponse<List<RouteDto>>("sip_get_topologia_linea", null, lineId);
            return routesDto;
        }

        private T GetResponse<T>(string action, string stopCode, int? lineId)
        {
            var client = new RestClient("https://tw.waw.pl/wp-admin/admin-ajax.php");
            var request = new RestRequest();
            request.Method = Method.POST;
            request.AddParameter("action", action);
            request.AddParameter("id", stopCode);
            request.AddParameter("idlinea", lineId);
            var result = client.Execute(request);

            T resultObj;

            try
            {

                if (result.Content == "Strona nie została wyświetlona z powodu istnienia konfliktu.")
                {
                    resultObj = GetResponse<T>(action, stopCode, lineId);
                }
                else
                {
                    resultObj = JsonConvert.DeserializeObject<T>(result.Content);
                }
            } catch
            {
                resultObj = this.GetResponse<T>(action, stopCode, lineId);
            }
            
            return resultObj;
        }
    }
}
