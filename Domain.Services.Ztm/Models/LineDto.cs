﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Services.Ztm.Models
{
    public class LineDto
    {
        [JsonProperty("IdLinea")]
        public int Id { get; set; }
        [JsonProperty("Codigo")]
        public string Number { get; set; }
        [JsonProperty("Nombre")]
        public string Route { get; set; }
    }
}
