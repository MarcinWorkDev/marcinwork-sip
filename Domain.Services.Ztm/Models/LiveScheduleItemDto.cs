﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Services.Ztm.Models
{
    public class LiveScheduleItemDto
    {
        [JsonProperty("Linea")]
        public string Line { get; set; }

        [JsonProperty("Destino")]
        public string Destination { get; set; }

        [JsonProperty("TiempoLlegada")]
        public TimeSpan ArrivalTime { get; set; }
        
        [JsonProperty("Minutos")]
        public short Minutes { get; set; }

        [JsonProperty("Propiedades")]
        public string Properties { get; set; }
    }
}
