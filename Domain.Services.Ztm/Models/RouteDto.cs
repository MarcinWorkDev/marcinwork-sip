﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Services.Ztm.Models
{
    public class RouteDto
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("Nombre")]
        public string Name { get; set; }
        [JsonProperty("ListaParadas")]
        public List<StopDto> Stops { get; set; }
        [JsonProperty("Origen")]
        public string Origin { get; set; }
        [JsonProperty("Destino")]
        public string Destination { get; set; }
    }
}
