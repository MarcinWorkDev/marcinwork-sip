﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Sip.Domain.Services.Ztm.Models
{
    public class StopDto
    {
        [JsonProperty("idParada")]
        public int Id { get; set; }

        [JsonProperty("codigo")]
        public string Code { get; set; }

        [JsonProperty("nombre")]
        public string Name { get; set; }

        [JsonProperty("latitud")]
        public decimal Latitude { get; set; }

        [JsonProperty("longitud")]
        public decimal Longitude { get; set; }

        [JsonProperty("nombreParada")]
        public string NameShort { get; set; }
    }
}
