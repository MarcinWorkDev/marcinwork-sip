﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MarcinWork.Sip.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using MarcinWork.Sip.Presentation.Website.Models;
using System.Security.Claims;

namespace MarcinWork.Sip.Presentation.Website.Controllers
{
    public class SipController : Controller
    {
        public IActionResult Stop(string stopCode)
        {
            return View();
        }

        public IActionResult LiveSchedule(string stopCode)
        {
            return PartialView("_LiveSchedule");
        }

        public IActionResult Search()
        {
            return View();
        }

        public IActionResult SearchLines()
        {
            return PartialView("_SearchLines");
        }

        public IActionResult SearchRoutes(int lineId)
        {
            return PartialView("_SearchRoutes");
        }

        public IActionResult SearchMap(decimal longMin, decimal longMax, decimal latMin, decimal latMax)
        {
            return PartialView("_SearchMap");
        }
        
        public IActionResult MyStops()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
