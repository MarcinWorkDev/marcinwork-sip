﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarcinWork.Sip.Domain.Data;
using MarcinWork.Sip.Domain.Interfaces;
using MarcinWork.Sip.Domain.Models;
using MarcinWork.Sip.Presentation.Website.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Website.Controllers
{
    [Authorize]
    public class MyStopController : BaseController
    {
        private readonly ISipRepository _sipRepository;

        public MyStopController(ISipRepository sipRepository) : base()
        {
            _sipRepository = sipRepository;
        }
        
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(DbStop))]
        public IActionResult GetMyStop(Guid id)
        {
            var myStop = _sipRepository.GetDbStop(GetCurrentUserId(), id);

            if (myStop == default(DbStop))
            {
                return NotFound();
            }

            return Ok(myStop);
        }
        
        [HttpPost]
        public IActionResult AddMyStop([FromBody]MyStopAddRequest request)
        {
            var myStop = _sipRepository.AddDbStop(GetCurrentUserId(), request.StopCode, request.Name);

            if (_externalSipClient.GetStop(request.StopCode, false) == null)
            {
                return BadRequest(new { Error = "Invalid stop code!" });
            }

            return Created(Url.Action("GetMyStop", "Stop", new { id =  myStop.Id}), myStop);
        }
        
        [HttpDelete("{id}")]
        public IActionResult DeleteMyStop(Guid id)
        {
            _sipRepository.DeleteStop(GetCurrentUserId(), id);
            return Ok();
        }
        
        [HttpPost("{id}/changeName")]
        public IActionResult ChangeNameMyStop(Guid id, [FromBody]MyStopChangeNameRequest request)
        {
            _sipRepository.ChangeNameDbStop(GetCurrentUserId(), id, request.Name);
            return Ok();
        }
    }
}