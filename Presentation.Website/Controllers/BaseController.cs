﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MarcinWork.Sip.Domain.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Presentation.Website.Controllers
{
    [Produces("application/json")]
    [Route("api/[Controller]")]
    public abstract class BaseController : Controller
    {
        private readonly IExternalSipClient __externalSipClient;
        private readonly ILogger __logger;

        protected IExternalSipClient _externalSipClient { get { return this.GetService(__externalSipClient); } }
        protected ILogger _logger { get { return this.GetService(__logger); } }

        private dynamic GetService<T>(T serviceInstance)
        {
            if (serviceInstance == null)
            {
                throw new Exception($"Not injected: {typeof(T).FullName}");
            } else
            {
                return serviceInstance;
            }
        }

        public BaseController(IExternalSipClient externalSipClient = null, ILogger logger = null)
        {
            __externalSipClient = externalSipClient;
            __logger = logger;
        }

        protected string GetCurrentUserId()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return null;
            }
            return User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
    }
}