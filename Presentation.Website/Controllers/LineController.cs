﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarcinWork.Sip.Domain.Interfaces;
using MarcinWork.Sip.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Website.Controllers
{
    public class LineController : BaseController
    {
        public LineController(IExternalSipClient externalSipClient) : base(externalSipClient) { }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<Line>))]
        public IActionResult Get()
        {
            var xx = _externalSipClient;
            return Ok(_externalSipClient.GetLines());
        }

        [HttpGet("{lineId}")]
        [ProducesResponseType(200, Type = typeof(Line))]
        public IActionResult Get(int lineId, bool withRoutes)
        {
            return Ok(_externalSipClient.GetLine(lineId, withRoutes));
        }

    }
}