﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarcinWork.Sip.Domain.Data;
using MarcinWork.Sip.Domain.Interfaces;
using MarcinWork.Sip.Domain.Models;
using MarcinWork.Sip.Presentation.Website.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Website.Controllers
{
    public class StopController : BaseController
    {
        public StopController() : base() { }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<Stop>))]
        public IActionResult Get()
        {
            var stops = _externalSipClient.GetStops();
            return Ok(stops);
        }
        
        [HttpPost("filter")]
        [ProducesResponseType(200, Type = typeof(List<Stop>))]
        public IActionResult GetFiltered([FromBody]StopMarkersRequest request)
        {
            try
            {
                var minLat = decimal.Parse(request.South.Replace(".",","));
                var maxLat = decimal.Parse(request.North.Replace(".", ","));
                var minLon = decimal.Parse(request.West.Replace(".", ","));
                var maxLon = decimal.Parse(request.South.Replace(".", ","));

                var stops = _externalSipClient.GetStops().Where(x => x.Latitude >= minLat && x.Latitude <= maxLat && x.Longitude >= minLon && x.Longitude <= maxLon).ToList();

                return Ok(stops);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("{stopCode}")]
        [ProducesResponseType(200, Type = typeof(Stop))]
        public IActionResult Get(string stopCode, bool withLiveSchedule)
        {
            var stop = _externalSipClient.GetStop(stopCode, withLiveSchedule);

            if (stop == default(Stop))
            {
                return NotFound();
            }

            return Ok(stop);
        }

        [ProducesResponseType(200, Type = typeof(List<LiveScheduleItem>))]
        [HttpGet("{stopCode}/live")]
        public IActionResult GetLiveSchedule(string stopCode)
        {
            return Ok(_externalSipClient.GetLiveSchedule(stopCode));
        }
    }
}