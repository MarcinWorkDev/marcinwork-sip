﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarcinWork.Sip.Presentation.Website.Models
{
    public class MyStopChangeNameRequest
    {
        public string Name { get; set; }
    }
}
