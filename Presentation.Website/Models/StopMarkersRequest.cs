﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarcinWork.Sip.Presentation.Website.Models
{
    public class StopMarkersRequest
    {
        public string South { get; set; }

        public string North { get; set; }

        public string East { get; set; }

        public string West { get; set; }

        public string Zoom { get; set; }
    }
}
